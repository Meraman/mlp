import cv2
img=cv2.imread('person2.jpg',cv2.IMREAD_COLOR)

gray_img=cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
#load the classifire
face_cascade=cv2.CascadeClassifier('./files/haarcascade_frontalface_default.xml')
faces=face_cascade.detectMultiScale(gray_img,1.1,4)#sclaing fectore,minimum nieghbours
#ploat detected object using classifire on the actual img
for (x,y,w,h) in faces:
	cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
cv2.imshow('facedetectore',img)
cv2.waitKey(3000)
cv2.destroyAllWindows()